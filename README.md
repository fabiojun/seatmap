# README #

### What is this software for? ###

* This software aims to help the meditation hall seats organization, allowing to put students information and placing in specific seats, according to known rules.
* Current version is a beta version

### How do I get set up? ###

* You need to install Java runtime 8 or later. Please [download it here.](https://java.com/download/) 
* After installing Java runtime for your operation system, just run the seatmap.jar file, double clicking it or executing it in a terminal.

### How I use it? ###
* Open a men or women CSV file, or start a blank layout and fill with student information
* Select Columns and Rows number, to match hall capacityto
* Select Men/Women side
* Block seats clicking the respective button in each seat
* Click sort button. The sort criteria is old student with lower age, new student lower age
* Move students clicking and holding the desired student to a new seat and releasing the mouse button
* Pin students and these will be ignored by the sort button
* Click Save button (it will be saved to a new file suffixed with date; the original file will not be overwritten)
* Print clicking the respective button

### Contribution guidelines ###

* Clone the repository
* Create a local branch
* Edit the source and commit
* Push your branch to upstream
* Create a pull request
* The code will be reviewed and merged to the master branch

