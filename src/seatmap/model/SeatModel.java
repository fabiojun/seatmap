/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author jun
 */
public class SeatModel extends Model {
    
    private StudentModel student;
    
    private IntegerProperty position;
    
    private BooleanProperty blocked;

    public SeatModel() {
        this.position = new SimpleIntegerProperty();
        this.blocked = new SimpleBooleanProperty();
    
    }
    
    /**
     * @return the student
     */
    public StudentModel getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(StudentModel student) {
        this.student = student;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position.get();
    }

    /**
     * @param position the position to set
     */
    public void setPosition(int position) {
        this.position.set(position);
    }

    public IntegerProperty positionProperty() {
        return this.position;
    }
    
    /**
     * @return the blocked
     */
    public boolean isBlocked() {
        return blocked.get();
    }

    /**
     * @param blocked the blocked to set
     */
    public void setBlocked(boolean blocked) {
        this.blocked.set(blocked);
    }
    
    public BooleanProperty blockedProperty() {
        return this.blocked;
    }
    
}
