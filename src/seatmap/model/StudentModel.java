/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author jun
 */
public class StudentModel extends Model implements Comparable {

    private final StringProperty name;

    private final IntegerProperty age;

    private final IntegerProperty courses;

    private final IntegerProperty prefSort;

    private final BooleanProperty fixed;

    private final BooleanProperty left;

    private StringProperty room;

    private SeatModel seat;

    public StudentModel() {
        name = new SimpleStringProperty("");
        age = new SimpleIntegerProperty();
        courses = new SimpleIntegerProperty();
        prefSort = new SimpleIntegerProperty();
        fixed = new SimpleBooleanProperty();
        left = new SimpleBooleanProperty();
        room = new SimpleStringProperty("");
    }

    public void clear() {
        this.setName("");
        this.setAge(0);
        this.setCourses(0);
        this.setRoom("");
    }

    @Override
    public int compareTo(Object compareStudent) {
        StudentModel cStudent = ((StudentModel) compareStudent);
        int compare = 0;
        if (this.isEmptyName() == cStudent.isEmptyName()) {
            compare = cStudent.getCourses() - this.getCourses();
            if (0 == compare) {
                //new student -> age asc
                if (cStudent.getCourses() == 0) {
                    compare = this.getAge() - cStudent.getAge();
                } //old student age desc
                else {
                    compare = cStudent.getAge() - this.getAge();
                }
            }

        } else if (this.isEmptyName() && !cStudent.isEmptyName()) {
            compare = 1;
        } else if (!this.isEmptyName() && cStudent.isEmptyName()) {
            compare = -1;
        }

        if (compare > 1) {
            compare = 1;
        }
        if (compare < -1) {
            compare = -1;
        }

        return compare;
    }

    public static List<StudentModel> prefSortStudents(List<StudentModel> students) {
        
//        for (int i = 0; i < students.size(); i++) {
        for (int i = students.size() -1; i >= 0; i--) {
            StudentModel student = students.get(i);
            if(student.getPrefSort() > 0 && student.getPrefSort() != (i +1)) {
                students.remove(student);
                students.add(student.getPrefSort()-1, student);
            }
        }
        
/*        
        Collections.sort(students, new Comparator<StudentModel>() {
            public int compare(StudentModel s1, StudentModel s2) {
                int compare = 0;
                
                if(s1.getPrefSort() == 0 && s2.getPrefSort() != 0 ) {
                    compare = 1;
                }
                else if(s1.getPrefSort() != 0 && s2.getPrefSort() == 0) {
                    compare = -1;
                }
                else if(s1.getPrefSort() == 0 && s2.getPrefSort() == 0){
                    compare = s1.compareTo(s2);
                }
                else {
                    if(s1.getPrefSort() > s2.getPrefSort()) {
                        compare = 1;
                    }
                    else {
                        compare = -1;
                    }
                }                
                return compare;
            }
        });
        */
        return students;
    }

    private boolean isEmptyName() {
        return (this.getName() == null || this.getName().equals(""));
    }

    public boolean isEmpty() {
        return ((this.getName() == null || this.getName().equals(""))
                && this.getAge() == 0 && this.getCourses() == 0);
    }

    public boolean isOldStudent() {
        boolean old = false;

        if (this.getCourses() > 0) {
            old = true;
        }

        return old;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Student name: ");
        sb.append(getName());
        sb.append("\nAge: ");
        sb.append(getAge());
        sb.append("\nCourses: ");
        sb.append(getCourses());
        sb.append("\nFixed: ");
        sb.append(isFixed());
        return sb.toString();
    }

    public String getFirstName() {
        String first = "";
        if (!this.getName().isEmpty()) {
            String names[] = this.getName().split(" ");
            if (names.length > 0) {
                first = names[0];
            }
        }
        return first;
    }

    public String getLastName() {
        StringBuilder last = new StringBuilder();
        if (!this.getName().isEmpty()) {
            String names[] = this.getName().split(" ");
            if (names.length > 0) {
                for (int i = 1; i < names.length; i++) {
                    last.append(names[i]);
                    last.append(" ");
                }
            }
        }
        return last.toString();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name.get();
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name.set(name.replaceAll("\"", ""));
    }

    public StringProperty nameProperty() {
        return name;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age.get();
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age.set(age);
    }

    public IntegerProperty ageProperty() {
        return age;
    }

    /**
     * @return the courses
     */
    public int getCourses() {
        return courses.get();
    }

    /**
     * @param courses the courses to set
     */
    public void setCourses(int courses) {
        this.courses.set(courses);
    }

    public IntegerProperty coursesProperty() {
        return courses;
    }

    /**
     * @return the courses
     */
    public int getPrefSort() {
        return prefSort.get();
    }

    /**
     * @param prefSort the courses to set
     */
    public void setPrefSort(int prefSort) {
        this.prefSort.set(prefSort);
    }

    public IntegerProperty prefSortProperty() {
        return prefSort;
    }

    public boolean isFixed() {
        return this.fixed.get();
    }

    /**
     * @param fixed the fixed to set
     */
    public void setFixed(boolean fixed) {
        this.fixed.set(fixed);
    }

    public BooleanProperty fixedProperty() {
        return fixed;
    }

    /**
     * @return the left
     */
    public boolean isLeft() {
        return left.get();
    }

    /**
     * @param left the left to set
     */
    public void setLeft(boolean left) {
        this.left.set(left);
    }

    public BooleanProperty leftProperty() {
        return left;
    }

    /**
     * @return the seat
     */
    public SeatModel getSeat() {
        return seat;
    }

    /**
     * @param seat the seat to set
     */
    public void setSeat(SeatModel seat) {
        this.seat = seat;
    }

    /**
     * @return the room
     */
    public String getRoom() {
        return room.get();
    }

    /**
     * @param room the room to set
     */
    public void setRoom(String room) {
        this.room.set(room.replaceAll("\"", ""));
    }

    public StringProperty roomProperty() {
        return room;
    }
}
