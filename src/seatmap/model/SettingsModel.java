/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author jun
 */
public class SettingsModel extends Model {

    public final static String STARTV_FROM_TOP = "Top";
    public final static String STARTV_FROM_BOTTOM = "Bottom";
    public final static String START_FROM_LEFT = "Men";
    public final static String START_FROM_RIGHT = "Women";
    
    private static SettingsModel instance = null;

    private IntegerProperty seatsPerRow;

    private IntegerProperty rows;

    private StringProperty startFrom;
    
    private StringProperty startVFrom;

    private BooleanProperty flip;
    
    private List<SortOptionModel> sortBy;

    private List<SortOptionModel> sortOptions;

    private double zoomLevel = 1;
    private double maxZoomLevel = 1.6;
    private double minZoomLevel = 0.2;


    private String csvChar = ",";

    protected SettingsModel() {
        sortBy = new ArrayList();
        sortOptions = new ArrayList();
        seatsPerRow = new SimpleIntegerProperty(5);
        rows = new SimpleIntegerProperty(10);
        startFrom = new SimpleStringProperty(START_FROM_LEFT);
        startVFrom = new SimpleStringProperty(STARTV_FROM_BOTTOM);
        flip = new SimpleBooleanProperty(false);
    }

    public static SettingsModel getInstance() {
        if (null == instance) {
            instance = new SettingsModel();
        }
        return instance;
    }

    /**
     * @return the seatsPerRow
     */
    public int getSeatsPerRow() {
        return seatsPerRow.get();
    }

    /**
     * @param seatsPerRow the seatsPerRow to set
     */
    public void setSeatsPerRow(int seatsPerRow) {
        this.seatsPerRow.set(seatsPerRow);
    }

    public IntegerProperty seatsPerRowProperty() {
        return this.seatsPerRow;
    }

    /**
     * @return the seatsQty
     */
    public int getSeatsQty() {
        return this.getRows() * this.getSeatsPerRow();
    }

    /**
     * @return the sortBy
     */
    public List<SortOptionModel> getSortBy() {
        return sortBy;
    }

    /**
     * @param sortBy the sortBy to set
     */
    public void setSortBy(List<SortOptionModel> sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * @return the sortOptions
     */
    public List<SortOptionModel> getSortOptions() {
        return sortOptions;
    }

    /**
     * @param sortOptions the sortOptions to set
     */
    public void setSortOptions(List<SortOptionModel> sortOptions) {
        this.sortOptions = sortOptions;
    }

    /**
     * @return the zoomLevel
     */
    public double getZoomLevel() {
        return zoomLevel;
    }

    /**
     * @param zoomLevel the zoomLevel to set
     */
    public void setZoomLevel(double zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    /**
     * @return the csvChar
     */
    public String getCsvChar() {
        return csvChar;
    }

    /**
     * @param csvChar the csvChar to set
     */
    public void setCsvChar(String csvChar) {
        this.csvChar = csvChar;
    }

    /**
     * @return the rows
     */
    public int getRows() {
        return rows.get();
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(int rows) {
        this.rows.set(rows);
    }

    public IntegerProperty rowsProperty() {
        return this.rows;
    }

    /**
     * @return the startFrom
     */
    public String getStartFrom() {
        return startFrom.get();
    }

    /**
     * @param startFrom the startFrom to set
     */
    public void setStartFrom(String startFrom) {
        this.startFrom.set(startFrom);
    }

    public StringProperty startFromProperty(){
        return this.startFrom;
    }

    /**
     * @return the maxZoomLevel
     */
    public double getMaxZoomLevel() {
        return maxZoomLevel;
    }

    /**
     * @param maxZoomLevel the maxZoomLevel to set
     */
    public void setMaxZoomLevel(double maxZoomLevel) {
        this.maxZoomLevel = maxZoomLevel;
    }

    /**
     * @return the minZoomLevel
     */
    public double getMinZoomLevel() {
        return minZoomLevel;
    }

    /**
     * @param minZoomLevel the minZoomLevel to set
     */
    public void setMinZoomLevel(double minZoomLevel) {
        this.minZoomLevel = minZoomLevel;
    }
    
    public void zoom(boolean zoomIn) {
         if (true == zoomIn) {
            this.zoomLevel += .2;
            if (this.getZoomLevel() > this.getMaxZoomLevel()) {
                this.zoomLevel = this.getMaxZoomLevel();
            }

        } else {
            this.zoomLevel -= .2;
            if (this.zoomLevel < this.getMinZoomLevel()) {
                this.zoomLevel = this.getMinZoomLevel();
            }
        }
    }

    /**
     * @return the startVFrom
     */
    public String getStartVFrom() {
        return startVFrom.get();
    }

    /**
     * @param startVFrom the startVFrom to set
     */
    public void setStartVFrom(String startVFrom) {
        this.startVFrom.set(startVFrom);
    }
    
    public StringProperty startVFromProperty() {
        return this.startVFrom;
    }

    /**
     * @return the flip
     */
    public BooleanProperty flipProperty() {
        return flip;
    }

    /**
     * @param flip the flip to set
     */
    public void setFlip(boolean flip) {
        this.flip.set(flip);
    }
    
    public boolean getFlip() {
        return this.flip.get();
    }
}
