/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import seatmap.SeatMap;
import seatmap.helper.Debug;

/**
 *
 * @author jun
 */
public class AppController extends Controller {
    
    private static String version = "1.0b2";

    private static AppController instance = null;

    private MenuController menuController;

    private ToolBarController toolBarController;

    private HallController hallController;

    private FileController fileController;

    private PrintController printController;
    
    private StatsController statsController;
    
    private AboutController aboutController;

    private Stage primaryStage;

    protected AppController(Stage primaryStage) throws Exception {

        if (null == primaryStage) {
            throw new Exception("Primary stage cannot be null");
        }
        this.primaryStage = primaryStage;

        menuController = new MenuController();
        toolBarController = new ToolBarController();
        hallController = new HallController();
        fileController = new FileController(primaryStage);
        printController = new PrintController();
        statsController = new StatsController();
        aboutController = new AboutController();

        MenuBar menuBar = menuController.getMenuBar();

        ToolBar toolBar = toolBarController.getToolBar();

        ScrollPane hall = hallController.getHall();

        VBox box = new VBox();

        box.getChildren().addAll(menuBar, toolBar, hall);
        Scene scene = new Scene(box);

        primaryStage.setMaximized(true);
        primaryStage.setTitle("SeatMap - " + version);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(SeatMap.class.getResource("/assets/css/seatmap.css").toExternalForm());
        
        this.setupShortcuts();
        
        primaryStage.show();

    }

    protected void setupShortcuts() {

        final KeyCombination keyComb = new KeyCodeCombination(KeyCode.W,
                KeyCombination.SHORTCUT_DOWN);
            this.getPrimaryStage().getScene().addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (keyComb.match(event)) {
                    getPrimaryStage().close();
                }
            }
        });
    }

    public static AppController getInstance(Stage primaryStage) {

        if (null == instance) {
            try {
                instance = new AppController(primaryStage);
            } catch (Exception e) {
                Debug.log(e.getMessage());
                e.printStackTrace();
            }

        }
        return instance;
    }

    /**
     * @return the menuController
     */
    public MenuController getMenuController() {
        return menuController;
    }

    /**
     * @param menuController the menuController to set
     */
    public void setMenuController(MenuController menuController) {
        this.menuController = menuController;
    }

    /**
     * @return the toolBarController
     */
    public ToolBarController getToolBarController() {
        return toolBarController;
    }

    /**
     * @param toolBarController the toolBarController to set
     */
    public void setToolBarController(ToolBarController toolBarController) {
        this.toolBarController = toolBarController;
    }

    /**
     * @return the hallController
     */
    public HallController getHallController() {
        return hallController;
    }

    /**
     * @param hallController the hallController to set
     */
    public void setHallController(HallController hallController) {
        this.hallController = hallController;
    }

    /**
     * @return the fileController
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * @param fileController the fileController to set
     */
    public void setFileController(FileController fileController) {
        this.fileController = fileController;
    }

    /**
     * @return the printController
     */
    public PrintController getPrintController() {
        return printController;
    }

    /**
     * @param printController the printController to set
     */
    public void setPrintController(PrintController printController) {
        this.printController = printController;
    }

    /**
     * @return the primaryStage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * @param primaryStage the primaryStage to set
     */
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    /**
     * @return the statsController
     */
    public StatsController getStatsController() {
        return statsController;
    }

    /**
     * @param statsController the statsController to set
     */
    public void setStatsController(StatsController statsController) {
        this.statsController = statsController;
    }

    /**
     * @return the aboutController
     */
    public AboutController getAboutController() {
        return aboutController;
    }

    /**
     * @param aboutController the aboutController to set
     */
    public void setAboutController(AboutController aboutController) {
        this.aboutController = aboutController;
    }
}
