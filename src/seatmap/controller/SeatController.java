/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import seatmap.helper.Debug;
import seatmap.model.SeatModel;
import seatmap.model.SettingsModel;
import seatmap.model.StudentModel;

/**
 *
 * @author jun
 */
public class SeatController extends Controller {

    private SeatModel seat;

    private StudentModel student;

    private Button clearBtn;

    private Button deleteBtn;

    private Label nameLabel;

    private TextField nameField;

    private Label ageLabel;

    private ComboBox ageCBO;

    private Label coursesLabel;

    private ComboBox coursesCBO;

    private Label prefSortLabel;

    private ComboBox prefSortCBO;

    private Label roomLabel;

    private TextField roomField;

    private Text nameText;

    private Text lastNameText;

    private Text infoText;

    private Text seatText;

    private Pane seatPane;

    private ToggleButton blockButton;

    private ToggleButton fixButton;

    private ToggleButton leftButton;

    private VBox studentBox;

    private VBox row0;

    private VBox row1;

    private HBox row2;

    private VBox row3;

    private static List<SeatController> selectedSeats = new ArrayList();

    private HallController hall;

    public SeatController(int position, HallController hall) {
        this.hall = hall;
        this.student = null;
        this.seat = new SeatModel();
        this.seat.setPosition(position);

        this.createSeatPane();
        this.createStudentPane();

        VBox vbox = new VBox();
        this.addStyleClass(vbox, "seat-vbox");

        HBox hbox = new HBox(
                this.getSeatText(),
                this.getBlockButton(),
                this.getFixButton(),
                this.getLeftButton()
        );
        this.addStyleClass(hbox, "seat-hbox");

        vbox.getChildren().addAll(hbox, (this.getStudentBox()));
        this.seatPane.getChildren().add(vbox);

        this.configEvents();
        this.updateStyle();

    }

    protected void createSeatPane() {
        this.seatPane = new StackPane();
//        this.seatPane.setMinWidth(180);
//        this.seatPane.setMinHeight(180);

        this.setSeatText(new Text());
        this.addStyleClass(this.getSeatText(), "seat-position");

        this.setBlockButton(new ToggleButton("Block"));
        this.setFixButton(new ToggleButton("Pin"));
        this.setLeftButton(new ToggleButton("Left"));

        this.addStyleClass(this.getBlockButton(), "block-toggle");
        this.addStyleClass(this.getFixButton(), "fixed-toggle");
        this.addStyleClass(this.getLeftButton(), "left-toggle");

        this.bindSeat();
    }

    protected void createStudentPane() {

        this.setClearBtn(new Button("Clr"));
        this.addStyleClass(this.getClearBtn(), "clear-button");
        this.setDeleteBtn(new Button("Del"));
        this.addStyleClass(this.getDeleteBtn(), "delete-button");
        this.setNameLabel(new Label("Student Name:"));
        this.setNameField(new TextField());
        this.setAgeLabel(new Label("Age:"));
        this.setAgeCBO(new ComboBox());
        this.addStyleClass(this.getAgeCBO(), "seat-combo-box");
        this.setCoursesLabel(new Label("Courses:"));
        this.setCoursesCBO(new ComboBox());
        this.addStyleClass(this.getCoursesCBO(), "seat-combo-box");
        this.setPrefSortLabel(new Label("PrefSeat:"));
        this.setPrefSortCBO(new ComboBox());
        this.addStyleClass(this.getPrefSortCBO(), "seat-combo-box");
        this.setRoomLabel(new Label("Room:"));
        this.setRoomField(new TextField());

        this.setNameText(new Text());
        this.addStyleClass(this.getNameText(), "first-name-text");
        this.setLastNameText(new Text());
        this.addStyleClass(this.getLastNameText(), "last-name-text");
        this.setInfoText(new Text());

        for (int i = 0; i < 50; i++) {
            this.getCoursesCBO().getItems().add(i);
        }

        for (int i = 8; i < 100; i++) {
            this.getAgeCBO().getItems().add(i);
        }

       this.updatePrefSortCBO();

        this.setStudentBox(new VBox());

        row0 = new VBox();
        row1 = new VBox();
        row2 = new HBox();
        row3 = new VBox();

        HBox col11 = new HBox();
        HBox col12 = new HBox();
        VBox col21 = new VBox();
        VBox col22 = new VBox();
        VBox col23 = new VBox();

        col12.getChildren().addAll(this.getClearBtn(), this.getDeleteBtn());
        col12.setAlignment(Pos.TOP_RIGHT);
        col11.getChildren().addAll(this.getNameLabel(), col12);
        col11.setAlignment(Pos.BASELINE_LEFT);
        HBox.setMargin(this.getClearBtn(), new Insets(0, 2, 0, 20));
        row1.getChildren().addAll(col11, this.getNameField());

        row0.getChildren().addAll(this.getNameText(), this.getLastNameText(),
                this.getInfoText());
        col21.getChildren().addAll(this.getAgeLabel(), this.getAgeCBO());
        col22.getChildren().addAll(this.getCoursesLabel(), this.getCoursesCBO());
        col23.getChildren().addAll(this.getPrefSortLabel(), this.getPrefSortCBO());
        getRow2().getChildren().addAll(col21, col22, col23);

        row3.getChildren().addAll(this.getRoomLabel(), this.getRoomField());
        getStudentBox().getChildren().addAll(row0, row1, row2, row3);

        this.getRow0().setVisible(false);
        this.getRow0().setManaged(false);

    }

    private void bindStudent() {

        if (null != this.getStudent()) {
            nameField.textProperty().bindBidirectional(this.getStudent().nameProperty());
            ageCBO.valueProperty().bindBidirectional(this.getStudent().ageProperty());
            coursesCBO.valueProperty().bindBidirectional(this.getStudent().coursesProperty());
            prefSortCBO.valueProperty().bindBidirectional(this.getStudent().prefSortProperty());
            leftButton.selectedProperty().bindBidirectional(this.getStudent().leftProperty());
            fixButton.selectedProperty().bindBidirectional(this.getStudent().fixedProperty());
            roomField.textProperty().bindBidirectional(this.getStudent().roomProperty());
            student.setSeat(this.getSeat());
        }
    }

    private void unbindStudent() {

        if (null != this.getStudent()) {
            nameField.textProperty().unbindBidirectional(this.getStudent().nameProperty());
            ageCBO.valueProperty().unbindBidirectional(this.getStudent().ageProperty());
            coursesCBO.valueProperty().unbindBidirectional(this.getStudent().coursesProperty());
            prefSortCBO.valueProperty().unbindBidirectional(this.getStudent().prefSortProperty());
            leftButton.selectedProperty().unbindBidirectional(this.getStudent().leftProperty());
            fixButton.selectedProperty().unbindBidirectional(this.getStudent().fixedProperty());
            roomField.textProperty().unbindBidirectional(this.getStudent().roomProperty());
            student.setSeat(null);
        }
    }

    private void bindSeat() {

        if (null != this.getSeat()) {
            this.getBlockButton().selectedProperty().bindBidirectional(this.getSeat().blockedProperty());
        }
    }

    private void unbindSeat() {

        if (null != this.getSeat()) {
            this.getBlockButton().selectedProperty().unbindBidirectional(this.getSeat().blockedProperty());
        }

    }

    public void clearStudent() {
        seat.getStudent().clear();
    }

    public void deleteStudent() {
        this.clearStudent();
        AppController app = AppController.getInstance(null);
        app.getHallController().getStudents().remove(this.getStudent());
        app.getHallController().createHall();
    }

    private void configEvents() {
        SeatController seat = this;
        seat.blockButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getHall().createHall();
                updateStyle();
                enableControls();
            }
        });
        seat.fixButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateStyle();
            }
        });
        seat.leftButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateStyle();
            }
        });

        seat.getClearBtn().setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                clearStudent();
                updateStyle();
            }
        });

        seat.getDeleteBtn().setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                deleteStudent();
            }
        });

        seat.coursesCBO.valueProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                seat.getStudent().setCourses((int) newValue);
                updateStyle();
            }
        });
        seat.nameField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                if (!newValue.trim().equals("")) {
                    updateStyle();
                    seat.configDragEvent();
                }
            }
        });
    }

    public void configDragEvent() {
        SeatController seat = this;
        if (!seat.isEmpty()) {
            seat.getSeatPane().setOnDragDetected(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Dragboard dragboard = getSeatPane().startDragAndDrop(TransferMode.MOVE);

                    if (canMove()) {
                        getSelectedSeats().clear();
                        getSelectedSeats().add(seat);

                        ClipboardContent cc = new ClipboardContent();
                        cc.putString(Integer.toString(seat.getPosition()));
                        dragboard.setContent(cc);
                        // JavaFX 8 only; Get only student fields panel
                        Image img = seat.getStudentBox().snapshot(null, null);
                        dragboard.setDragView(img, 0, 0);
                    }
                    event.consume();
                }
            });
        }
        seat.getSeatPane().setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard dragboard = event.getDragboard();
                boolean accept = false;
                if (dragboard.hasString()) {
                    String data = dragboard.getString();

                    try {
                        if (getSelectedSeats().size() > 0) {
                            SeatController draggedSeat = getSelectedSeats().get(0);
                            if (canMove()
                                    && draggedSeat.getPosition() != seat.getPosition()
                                    && event.getGestureSource() instanceof Pane) {
                                accept = true;
                            }
                        }
                    } catch (NumberFormatException exc) {
                        accept = false;
                    }
                }
                if (accept) {
                    event.acceptTransferModes(TransferMode.MOVE);
                }
            }
        });

        seat.getSeatPane().setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {

                if (getSelectedSeats().size() > 0 && canMove()) {
                    SeatController seatTo = seat;
                    SeatController seatFrom = getSelectedSeats().get(0);

                    getHall().moveStudent(seatTo, seatFrom);
                    getHall().createHall();
                }

            }
        });
    }

    public boolean canMove() {
        boolean canMove = false;
        if (!this.getStudent().isFixed() && !this.isBlocked()) {
            canMove = true;
        }
        return canMove;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return this.getSeat().getPosition();
    }

    /**
     * @return the student
     */
    public StudentModel getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(StudentModel student) {
        unbindStudent();
        this.student = student;
        this.student.setSeat(this.seat);
        this.seat.setStudent(student);
        bindStudent();

        this.updateStyle();
        this.enableControls();
    }

    public void enableControls() {

        boolean disabled = false;

        if (this.getSeat().isBlocked()) {
            disabled = true;
        }

        this.getClearBtn().setDisable(disabled);
        this.getDeleteBtn().setDisable(disabled);

        this.getNameField().setDisable(disabled);
        this.getAgeCBO().setDisable(disabled);
        this.getCoursesCBO().setDisable(disabled);
        this.getRoomField().setDisable(disabled);

        this.getFixButton().setDisable(disabled);
        this.getLeftButton().setDisable(disabled);
    }

    public void updateStyle() {
        String style = "";

        if (this.isBlocked()) {
            style = "blocked";
        } else if (this.isEmpty()) {
            style = "empty";
        } else if (null != this.getStudent()) {
            if (this.getStudent().isOldStudent()) {
                style = "old";
            } else {
                style = "new";
            }

            if (this.getStudent().isFixed()) {
                style = "fixed";
            }
            if (this.getStudent().isLeft()) {
                style = "left";
            }
        }
        this.addStyleClass(seatPane, "seat-pane");
        getStudentBox().getStyleClass().clear();
        getStudentBox().getStyleClass().addAll("student-pane", style);
    }

    public void printMode(boolean active) {

        boolean visible = !active;
        String eol = System.getProperty("line.separator");

        this.getBlockButton().setVisible(visible);
        this.getFixButton().setVisible(visible);
        this.getLeftButton().setVisible(visible);

        StringBuilder info = new StringBuilder();

        if (!this.getSeat().isBlocked() && !this.getStudent().isEmpty()) {
            this.getNameText().setText(this.getStudent().getFirstName() + ", "
                    + this.getStudent().getAge());
            this.getLastNameText().setText(this.getStudent().getLastName());

            if (this.getStudent().isOldStudent()) {
                info.append("OS");
            } else {
                info.append("NS");
            }
            if (!this.getStudent().getRoom().equals("")) {
                info.append(", " + this.getStudent().getRoom());
            }
        }
        this.getInfoText().setText(info.toString());

        this.getRow0().setVisible(!visible);
        this.getRow0().setManaged(!visible);
        this.getRow1().setVisible(visible);
        this.getRow1().setManaged(visible);
        this.getRow2().setVisible(visible);
        this.getRow2().setManaged(visible);
        this.getRow3().setVisible(visible);
        this.getRow3().setManaged(visible);

        if (active) {
            this.addStyleClass(seatPane, "printmode");
        } else {
            seatPane.getStyleClass().remove("printmode");
        }
    }

    /**
     * @return the nameField
     */
    public TextField getNameField() {
        return nameField;
    }

    /**
     * @param nameField the nameField to set
     */
    public void setNameField(TextField nameField) {
        this.nameField = nameField;
    }

    /**
     * @return the ageCBO
     */
    public ComboBox getAgeCBO() {
        return ageCBO;
    }

    /**
     * @param ageCBO the ageCBO to set
     */
    public void setAgeCBO(ComboBox ageCBO) {
        this.ageCBO = ageCBO;
    }

    /**
     * @return the coursesCBO
     */
    public ComboBox getCoursesCBO() {
        return coursesCBO;
    }

    /**
     * @param coursesCBO the coursesCBO to set
     */
    public void setCoursesCBO(ComboBox coursesCBO) {
        this.coursesCBO = coursesCBO;
    }

    /**
     * @return the seatText
     */
    public Text getSeatText() {
        return seatText;
    }

    /**
     * @param seatText the seatText to set
     */
    public void setSeatText(Text seatText) {
        this.seatText = seatText;
    }

    /**
     * @return the seatPane
     */
    public Pane getSeatPane() {
        return seatPane;
    }

    /**
     * @param seatPane the seatPane to set
     */
    public void setSeatPane(Pane seatPane) {
        this.seatPane = seatPane;
    }

    public boolean isEmpty() {
        return (null == student || student.isEmpty());
    }

    /**
     * @return the blockButton
     */
    public ToggleButton getBlockButton() {
        return blockButton;
    }

    /**
     * @param blockButton the blockButton to set
     */
    public void setBlockButton(ToggleButton blockButton) {
        this.blockButton = blockButton;
    }

    /**
     * @return the fixButton
     */
    public ToggleButton getFixButton() {
        return fixButton;
    }

    /**
     * @param fixButton the fixButton to set
     */
    public void setFixButton(ToggleButton fixButton) {
        this.fixButton = fixButton;
    }

    /**
     * @return the leftButton
     */
    public ToggleButton getLeftButton() {
        return leftButton;
    }

    /**
     * @param leftButton the leftButton to set
     */
    public void setLeftButton(ToggleButton leftButton) {
        this.leftButton = leftButton;
    }

    /**
     * @return the selectedSeats
     */
    public static List<SeatController> getSelectedSeats() {
        return selectedSeats;
    }

    /**
     * @param aSelectedSeats the selectedSeats to set
     */
    public static void setSelectedSeats(List<SeatController> aSelectedSeats) {
        selectedSeats = aSelectedSeats;
    }

    /**
     * @return the hall
     */
    public HallController getHall() {
        return hall;
    }

    /**
     * @param hall the hall to set
     */
    public void setHall(HallController hall) {
        this.hall = hall;
    }

    /**
     * @return the seat
     */
    public SeatModel getSeat() {
        return seat;
    }

    /**
     * @param seat the seat to set
     */
    public void setSeat(SeatModel seat) {
        this.unbindSeat();
        this.seat = seat;
        this.bindSeat();
    }

    /**
     * @return the roomField
     */
    public TextField getRoomField() {
        return roomField;
    }

    /**
     * @param roomField the roomField to set
     */
    public void setRoomField(TextField roomField) {
        this.roomField = roomField;
    }

    /**
     * @return the blocked
     */
    public boolean isBlocked() {
        return this.getSeat().isBlocked();
    }

    /**
     * @return the studentBox
     */
    public VBox getStudentBox() {
        return studentBox;
    }

    /**
     * @param studentBox the studentBox to set
     */
    public void setStudentBox(VBox studentBox) {
        this.studentBox = studentBox;
    }

    /**
     * @return the nameLabel
     */
    public Label getNameLabel() {
        return nameLabel;
    }

    /**
     * @param nameLabel the nameLabel to set
     */
    public void setNameLabel(Label nameLabel) {
        this.nameLabel = nameLabel;
    }

    /**
     * @return the ageLabel
     */
    public Label getAgeLabel() {
        return ageLabel;
    }

    /**
     * @return the coursesLabel
     */
    public Label getCoursesLabel() {
        return coursesLabel;
    }

    /**
     * @param coursesLabel the coursesLabel to set
     */
    public void setCoursesLabel(Label coursesLabel) {
        this.coursesLabel = coursesLabel;
    }

    /**
     * @return the roomLabel
     */
    public Label getRoomLabel() {
        return roomLabel;
    }

    /**
     * @param roomLabel the roomLabel to set
     */
    public void setRoomLabel(Label roomLabel) {
        this.roomLabel = roomLabel;
    }

    /**
     * @param ageLabel the ageLabel to set
     */
    public void setAgeLabel(Label ageLabel) {
        this.ageLabel = ageLabel;
    }

    /**
     * @return the clearBtn
     */
    public Button getClearBtn() {
        return clearBtn;
    }

    /**
     * @param clearBtn the clearBtn to set
     */
    public void setClearBtn(Button clearBtn) {
        this.clearBtn = clearBtn;
    }

    /**
     * @return the deleteBtn
     */
    public Button getDeleteBtn() {
        return deleteBtn;
    }

    /**
     * @param deleteBtn the deleteBtn to set
     */
    public void setDeleteBtn(Button deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    /**
     * @return the infoText
     */
    public Text getInfoText() {
        return infoText;
    }

    /**
     * @param infoText the infoText to set
     */
    public void setInfoText(Text infoText) {
        this.infoText = infoText;
    }

    /**
     * @return the nameText
     */
    public Text getNameText() {
        return nameText;
    }

    /**
     * @param nameText the nameText to set
     */
    public void setNameText(Text nameText) {
        this.nameText = nameText;
    }

    /**
     * @return the lastNameText
     */
    public Text getLastNameText() {
        return lastNameText;
    }

    /**
     * @param lastNameText the lastNameText to set
     */
    public void setLastNameText(Text lastNameText) {
        this.lastNameText = lastNameText;
    }

    /**
     * @return the row0
     */
    public VBox getRow0() {
        return row0;
    }

    /**
     * @param row0 the row0 to set
     */
    public void setRow0(VBox row0) {
        this.row0 = row0;
    }

    /**
     * @return the row1
     */
    public VBox getRow1() {
        return row1;
    }

    /**
     * @param row1 the row1 to set
     */
    public void setRow1(VBox row1) {
        this.row1 = row1;
    }

    /**
     * @return the row2
     */
    public HBox getRow2() {
        return row2;
    }

    /**
     * @param row2 the row2 to set
     */
    public void setRow2(HBox row2) {
        this.row2 = row2;
    }

    /**
     * @return the row3
     */
    public VBox getRow3() {
        return row3;
    }

    /**
     * @param row3 the row3 to set
     */
    public void setRow3(VBox row3) {
        this.row3 = row3;
    }

    /**
     * @return the prefSortLabel
     */
    public Label getPrefSortLabel() {
        return prefSortLabel;
    }

    /**
     * @param prefSortLabel the prefSortLabel to set
     */
    public void setPrefSortLabel(Label prefSortLabel) {
        this.prefSortLabel = prefSortLabel;
    }

    public void updatePrefSortCBO() {
        SettingsModel settings = SettingsModel.getInstance();
        for (int i = 1; i < settings.getSeatsQty() + 1; i++) {
            this.getPrefSortCBO().getItems().add(i);
        }

    }

    /**
     * @return the prefSortCBO
     */
    public ComboBox getPrefSortCBO() {
        return prefSortCBO;
    }

    /**
     * @param prefSortCBO the prefSortCBO to set
     */
    public void setPrefSortCBO(ComboBox prefSortCBO) {
        this.prefSortCBO = prefSortCBO;
    }

}
