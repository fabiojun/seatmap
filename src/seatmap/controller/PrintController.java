/*
 * Copyright (C) 2015 jun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Scale;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import seatmap.SeatMap;
import seatmap.helper.Debug;

/**
 *
 * @author jun
 */
public class PrintController extends Controller {

    private Stage printStage;

    private Stage dialogStage;

    private ToolBar toolbar;

    private PageLayout pageLayout;

    private Button pageSetupBtn;

    private Button printBtn;

    private Button cancelBtn;

    private ScrollPane preview;

    private Node printNode;

    private ImageView printImageView;

    private PrinterJob printerJob;

    public PrintController() {
        printStage = new Stage(StageStyle.DECORATED);
        dialogStage = new Stage(StageStyle.DECORATED);
        dialogStage.initOwner(printStage);

        pageSetupBtn = new Button("Page Setup");
        printBtn = new Button("Print");
        cancelBtn = new Button("Cancel");

        toolbar = new ToolBar();
        toolbar.getItems().addAll(pageSetupBtn, printBtn, cancelBtn);

        printImageView = new ImageView();

        VBox vbox = new VBox();
        vbox.getChildren().addAll(toolbar, printImageView);
        vbox.autosize();

        preview = new ScrollPane(vbox);
        preview.setFitToHeight(true);
        preview.setFitToWidth(true);
        preview.autosize();

        Scene scene = new Scene(preview);
        printStage.setScene(scene);
        scene.getStylesheets().add(SeatMap.class.getResource("/assets/css/seatmap.css").toExternalForm());
        printStage.setTitle("Print Preview");
        printStage.initModality(Modality.APPLICATION_MODAL);
        printStage.setMaximized(true);
        this.setupShortcuts();
    }

    /**
     * Scales the node based on the page layout.
     *
     * @param node The scene node to be printed.
     */
    public void print(final Node node) {

        this.setPrintNode(node);

        Printer printer = Printer.getDefaultPrinter();
        printerJob = PrinterJob.createPrinterJob();

        this.configEvents();

        if (null == pageLayout) {
            pageLayout = printer.createPageLayout(
                    Paper.A4,
                    PageOrientation.PORTRAIT,
                    Printer.MarginType.DEFAULT
            );
            printerJob.getJobSettings().setPageLayout(pageLayout);
        }

        this.printPreview();
        printStage.show();
        this.printPreview();
    }

    public void printPreview() {

        printNode.getTransforms().clear();

        AppController app = AppController.getInstance(null);
        app.getHallController().printMode(true);

        double scaleX = pageLayout.getPrintableWidth() / printNode.getBoundsInParent().getWidth();
        double scaleY = pageLayout.getPrintableHeight() / printNode.getBoundsInParent().getHeight();
        
        if (scaleX > scaleY) {
            scaleX = scaleY;
        } else {
            scaleY = scaleX;
        }

        printNode.getTransforms().add(new Scale(scaleX, scaleY));

        Image img = printNode.snapshot(null, null);

        printImageView.setImage(img);
        printImageView.relocate(0, 0);
        preview.setPrefSize(img.getWidth(), img.getHeight());

    }

    public void configEvents() {

        pageSetupBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                boolean showDialog = printerJob.showPageSetupDialog(dialogStage);

                if (showDialog) {
                    pageLayout = printerJob.getJobSettings().getPageLayout();
                    printPreview();
                }
            }
        });

        printBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                boolean showDialog = printerJob.showPrintDialog(dialogStage);

                if (showDialog) {
                    if (printerJob != null) {
                        boolean success = printerJob.printPage(pageLayout, printNode);
                        if (success) {
                            printerJob.endJob();
                        }
                    }
                    closePrintPreview();
                }
            }
        });

        cancelBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                closePrintPreview();
            }
        });

        printStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent Event) {
                closePrintPreview();
            }
        });
    }

    protected void setupShortcuts() {

        final KeyCombination keyComb = new KeyCodeCombination(KeyCode.W,
                KeyCombination.SHORTCUT_DOWN);
        this.printStage.getScene().addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (keyComb.match(event)) {
                    closePrintPreview();
                }
            }
        });
    }

    public void closePrintPreview() {
        AppController app = AppController.getInstance(null);
        app.getHallController().printMode(false);

        printNode.getTransforms().clear();
        printStage.hide();
    }

    /**
     * @return the printNode
     */
    public Node getPrintNode() {
        return printNode;
    }

    /**
     * @param printNode the printNode to set
     */
    public void setPrintNode(Node printNode) {
        this.printNode = printNode;
    }

    /**
     * @return the printImageView
     */
    public ImageView getPrintImageView() {
        return printImageView;
    }

    /**
     * @param printImageView the printImageView to set
     */
    public void setPrintImageView(ImageView printImageView) {
        this.printImageView = printImageView;
    }
}
