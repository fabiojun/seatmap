/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import seatmap.SeatMap;
import seatmap.helper.Debug;
import seatmap.model.SeatModel;
import seatmap.model.SettingsModel;
import seatmap.model.StudentModel;

/**
 *
 * @author jun
 */
public class FileController extends Controller {

    public final static String CSV_EXTENSION = ".csv";
    public final static String SM_EXTENSION = ".csvx";

    private String loadedFilename;

    private String loadedExtension;

    private String saveToFilename;

    private String path;

    private FileChooser fileChooser;

    private Stage stage;

    private Stage dialogStage;

    private Text dialogText;
    
    public FileController(Stage stage) {
        this.stage = stage;
        fileChooser = new FileChooser();
    }

    public void openFile() {
        List<StudentModel> students;
        File selectedFile = fileChooser.showOpenDialog(stage);
        fileChooser.setTitle("Open From File");

        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Comma Separated Values files",
                        "*" + FileController.CSV_EXTENSION, "*" + FileController.SM_EXTENSION)
        );

        if (null != selectedFile) {
            this.setLoadedFilename(selectedFile.getName());
            this.setPath(selectedFile.getParent());
            this.showLoadedDialog(selectedFile.getName());

            if (this.getLoadedExtension().endsWith(FileController.CSV_EXTENSION)) {
                this.loadFromCSV(selectedFile);
            } else if (this.getLoadedExtension().endsWith(FileController.SM_EXTENSION)) {
                this.loadFromCSVX(selectedFile);
            }
        }
    }

    public void loadFromCSV(File file) {
        List<StudentModel> students = new ArrayList();
        StringBuilder sb = new StringBuilder(1024);
        String curLine = null;

        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String csvChar = SettingsModel.getInstance().getCsvChar();
            do {
                curLine = br.readLine();
                if (null != curLine) {
                    String[] fields = curLine.split(csvChar);
                    if (fields.length >= 3) {
                        StudentModel student = new StudentModel();
                        student.setName(fields[0]);
                        student.setAge(Integer.parseInt(fields[1]));
                        student.setCourses(Integer.parseInt(fields[2]));
                        students.add(student);
                    }
                }
            } while (curLine != null);
            HallController hall = AppController.getInstance(null).getHallController();
            hall.setStudents(students);
            hall.clearSeats();
            hall.createHall();
            hall.scrollToBottom();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadFromCSVX(File file) {
        List<StudentModel> students = new ArrayList();

        StringBuilder sb = new StringBuilder(1024);
        String curLine = null;

        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String csvChar = SettingsModel.getInstance().getCsvChar();
            HallController hall = AppController.getInstance(null).getHallController();
            Map<Integer, SeatController> seats = new HashMap<>();
            SettingsModel settings = SettingsModel.getInstance();
            int line = 0;

            do {
                curLine = br.readLine();
                if (null != curLine) {
                    line++;
                    String[] fields = curLine.split(csvChar);
                    if (1 == line && fields.length == 4) {
                        try {
                            settings.setRows(Integer.parseInt(fields[0]));
                            settings.setSeatsPerRow(Integer.parseInt(fields[1]));
                            settings.setStartVFrom(fields[2]);
                            settings.setStartFrom(fields[3]);
                        } catch (Exception e) {
                            Debug.log(e.getMessage());
                        }
                    } else {
                        if (fields.length >= 8) {
                            StudentModel student = new StudentModel();
                            student.setName(fields[0]);
                            student.setAge(Integer.parseInt(fields[1]));
                            student.setCourses(Integer.parseInt(fields[2]));

                            SeatModel seat = new SeatModel();
                            int position = Integer.parseInt(fields[3]);
                            seat.setPosition(position);

                            seat.setBlocked(Boolean.parseBoolean(fields[4]));
                            student.setFixed(Boolean.parseBoolean(fields[5]));
                            student.setLeft(Boolean.parseBoolean(fields[6]));
                            student.setRoom(fields[7]);

                            student.setSeat(seat);

                            students.add(student);

                            SeatController seatController = new SeatController(position, hall);
                            seatController.setSeat(seat);
                            seatController.setStudent(student);
                            seats.put(position, seatController);

                        }
                    }
                }
            } while (curLine != null);

            hall.setStudents(students);
            hall.setSeats(seats);
            hall.createHall();
            hall.scrollToBottom();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void save() {

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String filename;
        if (null != this.getLoadedFilename() && !this.getLoadedFilename().equals("")) {
            filename = this.getLoadedFilename();
            filename += "_" + format.format(today) + FileController.SM_EXTENSION;
        } else {
            filename = this.saveAsDialog();
//            if (null == filename) {
//                if (this.getPath() == null) {
//                    this.setPath(System.getProperty("user.dir") + File.separator);
//                }
//                filename = "students_" + format.format(today) + FileController.SM_EXTENSION;
//
//            }
        }
        this.setSaveToFilename(filename);
        if (null != filename) {
            this.saveToFile();
        }

    }

    public void saveAs() {

        if (null != this.saveAsDialog()) {
            this.saveToFile();
        }
    }

    public String saveAsDialog() {
        String filename = null;

        fileChooser.setTitle("Save to File");

        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Comma Separated Values files",
                        "*" + FileController.SM_EXTENSION)
        );
        File selectedFile = fileChooser.showSaveDialog(stage);

        if (null != selectedFile) {
            filename = selectedFile.getName();
            this.setPath(selectedFile.getParent());
        }

        this.setSaveToFilename(filename);
        this.setLoadedFilename(filename);
        return filename;
    }

    public void saveToFile() {
        HallController hall = AppController.getInstance(null).getHallController();
        Map<Integer, SeatController> seats = hall.getSeats();
        SettingsModel settings = SettingsModel.getInstance();

        try {
            String fullPath = this.getPath() + File.separator + this.getSaveToFilename();
            Debug.log("Saving to: " + fullPath);
            FileWriter file = new FileWriter(fullPath);
            BufferedWriter bf = new BufferedWriter(file);

            String csvChar = SettingsModel.getInstance().getCsvChar();
            String quotes = "\"";

            String line = settings.getRows() + csvChar
                    + settings.getSeatsPerRow() + csvChar
                    + settings.getStartVFrom() + csvChar
                    + settings.getStartFrom() + csvChar;

            bf.write(line);
            bf.newLine();

            for (int i = 1; i <= seats.size(); i++) {
                SeatController seatController = seats.get(i);
                SeatModel seat = seatController.getSeat();
                StudentModel student = seatController.getStudent();

                line = quotes + student.getName().replaceAll(quotes, "") + quotes + csvChar
                        + student.getAge() + csvChar
                        + student.getCourses() + csvChar
                        + seat.getPosition() + csvChar
                        + seat.isBlocked() + csvChar
                        + student.isFixed() + csvChar
                        + student.isLeft() + csvChar
                        + quotes + student.getRoom() + quotes;

                bf.write(line);
                bf.newLine();

            }
            bf.flush();
            bf.close();

            this.showSavedDialog(fullPath);

        } catch (Exception e) {
            Debug.log(e.getMessage());
            e.printStackTrace();
        }
    }

    public void export() {

        String filename = null;
        String path = null;
        String fullPath = null;

        fileChooser.setTitle("Save to File");

        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Text Files", "*.txt")
        );
        File selectedFile = fileChooser.showSaveDialog(stage);

        if (null != selectedFile) {
            filename = selectedFile.getName();
            path = selectedFile.getParent();
        }

        if (null != filename && null != path) {
            fullPath = path + File.separator + filename;
            HallController hall = AppController.getInstance(null).getHallController();
            Map<Integer, SeatController> seats = hall.getSeats();

            try {
                Debug.log("Export to: " + fullPath);
                FileWriter file = new FileWriter(fullPath);
                BufferedWriter bf = new BufferedWriter(file);
                String quotes = "\"";
                String line;

                for (int i = 1; i <= seats.size(); i++) {
                    SeatController seatController = seats.get(i);
                    SeatModel seat = seatController.getSeat();
                    StudentModel student = seatController.getStudent();

                    if (student.isEmpty()) {
                        line = seat.getPosition() + " - Empty seat";
                    } else {
                        line = seat.getPosition() + " - "
                                + student.getName().replaceAll(quotes, "");
                    }

                    bf.write(line);
                    bf.newLine();

                }
                bf.flush();
                bf.close();

                this.showSavedDialog(fullPath);

            } catch (Exception e) {
                Debug.log(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void showSavedDialog(String fullPath) {
        this.showDialog("Saved to File:", fullPath);
    }

    public void showLoadedDialog(String fullPath) {
        this.showDialog("Loading from File", fullPath);
    }

    public void showDialog(String title, String fullPath) {

        this.loadDialogStage();
        this.dialogStage.setTitle(title);
        this.dialogText.setText(fullPath);

        this.dialogStage.show();

        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(4000),
                ae -> this.dialogStage.hide()));
        timeline.play();

    }

    /**
     * @return the dialogStage
     */
    public Stage loadDialogStage() {
        if (null == this.dialogStage) {
            this.dialogStage = new Stage();
            VBox vbox = new VBox();
            Button okBtn = new Button("OK");
            dialogText = new Text();
            vbox.getChildren().addAll(dialogText, okBtn);
            this.addStyleClass(vbox, "saved-dialog");
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.initOwner(this.stage);
            dialogStage.setMinWidth(300);
            dialogStage.setMinHeight(100);

            Scene scene = new Scene(vbox);
            scene.getStylesheets().add(SeatMap.class.getResource("/assets/css/seatmap.css").toExternalForm());
            dialogStage.setScene(scene);

            okBtn.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    dialogStage.hide();
                }
            });

        }
        return this.dialogStage;
    }

    /**
     * @return the loadedFilename
     */
    public String getLoadedFilename() {
        return loadedFilename;
    }

    /**
     * @param loadedFilename the loadedFilename to set
     */
    public void setLoadedFilename(String loadedFilename) {

        if (null != loadedFilename) {
            if (loadedFilename.endsWith(FileController.CSV_EXTENSION)) {
                this.setLoadedExtension(FileController.CSV_EXTENSION);
                loadedFilename = loadedFilename.replace(FileController.CSV_EXTENSION, "");
            } else if (loadedFilename.endsWith(FileController.SM_EXTENSION)) {
                this.setLoadedExtension(FileController.SM_EXTENSION);
                loadedFilename = loadedFilename.replace(FileController.SM_EXTENSION, "");
            }

            if (loadedFilename.length() > 10) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String dts = loadedFilename.substring(
                        loadedFilename.length() - 10,
                        loadedFilename.length()
                );

                try {
                    Date dt = format.parse(dts);
                    if (dt != null) {
                        loadedFilename = loadedFilename.substring(0, loadedFilename.length() - 11);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        this.loadedFilename = loadedFilename;
    }

    /**
     * @return the saveToFilename
     */
    public String getSaveToFilename() {
        return saveToFilename;
    }

    /**
     * @param saveToFilename the saveToFilename to set
     */
    public void setSaveToFilename(String saveToFilename) {
        this.saveToFilename = saveToFilename;
    }

    /**
     * @return the fileChooser
     */
    public FileChooser getFileChooser() {
        return fileChooser;
    }

    /**
     * @param fileChooser the fileChooser to set
     */
    public void setFileChooser(FileChooser fileChooser) {
        this.fileChooser = fileChooser;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the loadedExtension
     */
    public String getLoadedExtension() {
        return loadedExtension;
    }

    /**
     * @param loadedExtension the loadedExtension to set
     */
    public void setLoadedExtension(String loadedExtension) {
        this.loadedExtension = loadedExtension;
    }

}
