/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import seatmap.helper.Debug;

/**
 *
 * @author jun
 */
public class MenuController extends Controller {

    private MenuBar menuBar;

    public MenuController() {
        menuBar = new MenuBar();

        Menu menuFile = new Menu("File");
        MenuItem newFile = new MenuItem("New");
        MenuItem openFile = new MenuItem("Open");
        MenuItem save = new MenuItem("Save");
        MenuItem saveAs = new MenuItem("Save As...");
        MenuItem export = new MenuItem("Export...");
        MenuItem exitApp = new MenuItem("Exit");
        SeparatorMenuItem separator = new SeparatorMenuItem();

        menuFile.getItems().addAll(newFile, openFile, save, saveAs, export,
                separator, exitApp);

        //@todo further versions
//        Menu menuSettings = new Menu("Settings");
        Menu menuStats = new Menu("Stats");
        MenuItem showStats = new MenuItem("Show stats");
        menuStats.getItems().addAll(showStats);
        
        Menu menuAbout = new Menu("About");
        MenuItem about = new MenuItem("About...");
        menuAbout.getItems().addAll(about);

        menuBar.getMenus().addAll(menuFile, menuStats, menuAbout);

        newFile.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getHallController().clearSeats();
                app.getHallController().clearStudents();
                app.getHallController().createHall();
                app.getFileController().setLoadedFilename(null);
            }
        });

        openFile.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getFileController().openFile();
            }
        });

        save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getFileController().save();
            }
        });

        saveAs.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getFileController().saveAs();
            }
        });

        export.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getFileController().export();
            }
        });

        exitApp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                System.exit(0);
            }
        });

        showStats.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getStatsController().showStats();
                Debug.log("tst");
            }
        });
        
        about.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               AppController app = AppController.getInstance(null);
               app.getAboutController().showAbout();
            }
        });
    }

    /**
     * @return the menuBar
     */
    public MenuBar getMenuBar() {
        return menuBar;
    }

    /**
     * @param menuBar the menuBar to set
     */
    public void setMenuBar(MenuBar menuBar) {
        this.menuBar = menuBar;
    }
}
