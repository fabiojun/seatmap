/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.text.Text;
import seatmap.helper.Debug;
import seatmap.model.SettingsModel;

/**
 *
 * @author jun
 */
public class ToolBarController extends Controller {

    private ToolBar toolBar;

    private Button openFileBtn;

    private Button saveBtn;

    private Button printBtn;

    private Button sortBtn;
    
    private Button prefSortBtn;

    private Button zoomInBtn;

    private Button zoomOutBtn;

    private Button statsBtn;

    private ToggleButton flipBtn;

    private ComboBox colsCB;

    private ComboBox rowsCB;

    private Text colText;

    private Text rowText;

    private Text sideText;

    private ComboBox startFromCB;

    public ToolBarController() {
        toolBar = new ToolBar();
        openFileBtn = new Button("Open");
        saveBtn = new Button("Save");
        printBtn = new Button("Print");
        sortBtn = new Button("Sort");
        prefSortBtn = new Button("PrefSort");
        zoomInBtn = new Button("Zoom+");
        zoomOutBtn = new Button("Zoom-");
        statsBtn = new Button("Stats");
        flipBtn = new ToggleButton("Flip");

        colsCB = new ComboBox();
        rowsCB = new ComboBox();
        for (int i = 1; i <= 20; i++) {
            colsCB.getItems().add(i);
            rowsCB.getItems().add(i);
        }

        colText = new Text("Columns");
        rowText = new Text("Rows");

        sideText = new Text("Side");

        startFromCB = new ComboBox();
        startFromCB.getItems().addAll(
                SettingsModel.START_FROM_LEFT,
                SettingsModel.START_FROM_RIGHT
        );

        this.configEvents();
        toolBar.getItems().addAll(openFileBtn, saveBtn, printBtn, colText,
                colsCB, rowText, rowsCB, sideText, startFromCB, flipBtn,
                sortBtn, prefSortBtn, zoomInBtn, zoomOutBtn, statsBtn);

    }

    public void configEvents() {

        SettingsModel settings = SettingsModel.getInstance();

        colsCB.valueProperty().bindBidirectional(settings.seatsPerRowProperty());
        rowsCB.valueProperty().bindBidirectional(settings.rowsProperty());
        startFromCB.valueProperty().bindBidirectional(settings.startFromProperty());
        flipBtn.selectedProperty().bindBidirectional(settings.flipProperty());

        openFileBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getFileController().openFile();
            }
        });

        saveBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getFileController().save();
            }
        });

        printBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getPrintController().print(app.getHallController().getHallPane());
            }
        });

        sortBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getHallController().getStudents().sort(null);
                app.getHallController().createHall();
            }
        });

        prefSortBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getHallController().prefSortStudents();
                app.getHallController().createHall();
            }
        });
        
        zoomInBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                SettingsModel.getInstance().zoom(true);
                AppController app = AppController.getInstance(null);
                app.getHallController().zoom();
            }
        });

        zoomOutBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                SettingsModel.getInstance().zoom(false);
                AppController app = AppController.getInstance(null);
                app.getHallController().zoom();
            }
        });

        statsBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getStatsController().showStats();
            }
        });

        colsCB.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                AppController app = AppController.getInstance(null);
                app.getHallController().createHall();

            }
        });

        rowsCB.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                AppController app = AppController.getInstance(null);
                app.getHallController().createHall();
            }
        });

        startFromCB.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                AppController app = AppController.getInstance(null);
                app.getHallController().createHall();
            }
        });

        flipBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent Event) {
                AppController app = AppController.getInstance(null);
                app.getHallController().createHall();
            }
        });
    }

    /**
     * @return the toolBar
     */
    public ToolBar getToolBar() {
        return toolBar;
    }

    /**
     * @param toolBar the toolBar to set
     */
    public void setToolBar(ToolBar toolBar) {
        this.toolBar = toolBar;
    }
}
