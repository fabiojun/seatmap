/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import java.net.URI;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import seatmap.SeatMap;

/**
 *
 * @author jun
 */
public class AboutController extends Controller {

    private Stage aboutStage;

    private VBox aboutBox;

    private Text aboutText;

    public AboutController() {
    }

    public void init() {

        if (null == this.aboutStage) {
            aboutStage = new Stage(StageStyle.DECORATED);
            aboutBox = new VBox();
            aboutText = new Text(this.aboutText());
            
            aboutBox.getStyleClass().add("about-box");
            
            Hyperlink srcLink = new Hyperlink("https://bitbucket.org/fabiojun/seatmap/");
            aboutBox.getChildren().addAll(aboutText, srcLink);
            
            srcLink.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    try {
                        java.awt.Desktop.getDesktop().browse(new URI(srcLink.getText()));
                    } catch (Exception e) {
                    }
                    
                }
            });

            Scene scene = new Scene(aboutBox);
            aboutStage.setScene(scene);
            scene.getStylesheets().add(SeatMap.class.getResource("/assets/css/seatmap.css").toExternalForm());
            aboutStage.setTitle("About");
            aboutStage.initModality(Modality.APPLICATION_MODAL);

        }
    }

    public void showAbout() {
        this.init();
        this.getAboutStage().show();
    }

    public String aboutText() {
        StringBuffer sb = new StringBuffer();
        
        sb.append("\n"
                + " Copyright (C) 2015 Vipassana São Paulo-Brazil\n"
                + " \n"
                + " This program is free software: you can redistribute it and/or modify\n"
                + " it under the terms of the GNU General Public License as published by\n"
                + " the Free Software Foundation, either version 3 of the License, or\n"
                + " (at your option) any later version.\n"
                + " \n"
                + " This program is distributed in the hope that it will be useful,\n"
                + " but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                + " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                + " GNU General Public License for more details.\n"
                + " \n"
                + " You should have received a copy of the GNU General Public License\n"
                + " along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
                + " \n" 
                + " ===============================================\n\n"
                + " You can colaborate with the development and get the source code in the link below:\n" 
        );

        return sb.toString();
    }

    /**
     * @return the aboutStage
     */
    public Stage getAboutStage() {
        return aboutStage;
    }

    /**
     * @param aboutStage the aboutStage to set
     */
    public void setAboutStage(Stage aboutStage) {
        this.aboutStage = aboutStage;
    }

    /**
     * @return the aboutBox
     */
    public VBox getAboutBox() {
        return aboutBox;
    }

    /**
     * @param aboutBox the aboutBox to set
     */
    public void setAboutBox(VBox aboutBox) {
        this.aboutBox = aboutBox;
    }

    /**
     * @return the aboutText
     */
    public Text getAboutText() {
        return aboutText;
    }

    /**
     * @param aboutText the aboutText to set
     */
    public void setAboutText(Text aboutText) {
        this.aboutText = aboutText;
    }
}
