/*
 * Copyright (C) 2015 jun
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.converter.NumberStringConverter;
import seatmap.SeatMap;

/**
 *
 * @author jun
 */
public class StatsController extends Controller {

    private Stage statsStage;

    private GridPane pane;

    private Text totalSeatsText;

    private Text oldStudentsText;
    private Text newStudentsText;
    private Text leftStudentsText;
    private Text blockedSeatsText;
    private Text pinnedSeatsText;
    private Text emptySeatsText;

    public StatsController() {

    }

    public void init() {

        if (null == this.statsStage) {
            statsStage = new Stage(StageStyle.DECORATED);

            pane = new GridPane();
            pane.setMinHeight(200);
            pane.setMinWidth(200);
            pane.getStyleClass().add("stats-pane");

            pane.setHgap(10);
            pane.setVgap(10);
            pane.setPadding(new Insets(25, 25, 25, 25));

            Scene scene = new Scene(pane);
            statsStage.setScene(scene);
            scene.getStylesheets().add(SeatMap.class.getResource("/assets/css/seatmap.css").toExternalForm());
            statsStage.setTitle("Stats");
            statsStage.initModality(Modality.APPLICATION_MODAL);

            this.setupShortcuts();

            HallController hall = AppController.getInstance(null).getHallController();

            this.setTotalSeatsText(new Text());
            this.getTotalSeatsText().textProperty().bindBidirectional(
                    hall.totalSeatsProperty(), new NumberStringConverter());

            this.setBlockedSeatsText(new Text());
            this.getBlockedSeatsText().textProperty().bindBidirectional(
                    hall.blockedSeatsProperty(), new NumberStringConverter());

            this.setEmptySeatsText(new Text());
            this.getEmptySeatsText().textProperty().bindBidirectional(
                    hall.emptySeatsProperty(), new NumberStringConverter());

            this.setLeftStudentsText(new Text());
            this.getLeftStudentsText().textProperty().bindBidirectional(
                    hall.leftStudentsProperty(), new NumberStringConverter());

            this.setOldStudentsText(new Text());
            this.getOldStudentsText().textProperty().bindBidirectional(
                    hall.oldStudentsProperty(), new NumberStringConverter());

            this.setNewStudentsText(new Text());
            this.getNewStudentsText().textProperty().bindBidirectional(
                    hall.newStudentsProperty(), new NumberStringConverter());

            this.setPinnedSeatsText(new Text());
            this.getPinnedSeatsText().textProperty().bindBidirectional(
                    hall.pinnedStudentsProperty(), new NumberStringConverter());

            int col = 1;
            int row = 1;

            pane.add(new Text("Total Seats: "), col, row);
            col++;
            pane.add(this.getTotalSeatsText(), col, row);

            row++;
            col = 1;
            pane.add(new Text("Empty Seats: "), col, row);
            col++;
            pane.add(this.getEmptySeatsText(), col, row);

            row++;
            col = 1;
            pane.add(new Text("Old Students: "), col, row);
            col++;
            pane.add(this.getOldStudentsText(), col, row);

            row++;
            col = 1;
            pane.add(new Text("New Students: "), col, row);
            col++;
            pane.add(this.getNewStudentsText(), col, row);

            row++;
            col = 1;
            pane.add(new Text("Left Students: "), col, row);
            col++;
            pane.add(this.getLeftStudentsText(), col, row);

            row++;
            col = 1;
            pane.add(new Text("Blocked Seats: "), col, row);
            col++;
            pane.add(this.getBlockedSeatsText(), col, row);

            row++;
            col = 1;
            pane.add(new Text("Pinned Students: "), col, row);
            col++;
            pane.add(this.getPinnedSeatsText(), col, row);

        }
    }

    public void showStats() {

        this.init();
        AppController app = AppController.getInstance(null);
        app.getHallController().updateStatsProperties();

        this.getStatsStage().show();
    }

    protected void setupShortcuts() {

        final KeyCombination keyComb = new KeyCodeCombination(KeyCode.W,
                KeyCombination.SHORTCUT_DOWN);
        this.statsStage.getScene().addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (keyComb.match(event)) {
                    statsStage.hide();
                }
            }
        });
    }

    /**
     * @return the statsStage
     */
    public Stage getStatsStage() {
        return statsStage;
    }

    /**
     * @param statsStage the statsStage to set
     */
    public void setStatsStage(Stage statsStage) {
        this.statsStage = statsStage;
    }

    /**
     * @return the totalSeatsText
     */
    public Text getTotalSeatsText() {
        return totalSeatsText;
    }

    /**
     * @param totalSeatsText the totalSeatsText to set
     */
    public void setTotalSeatsText(Text totalSeatsText) {
        this.totalSeatsText = totalSeatsText;
    }

    /**
     * @return the oldStudentsText
     */
    public Text getOldStudentsText() {
        return oldStudentsText;
    }

    /**
     * @param oldStudentsText the oldStudentsText to set
     */
    public void setOldStudentsText(Text oldStudentsText) {
        this.oldStudentsText = oldStudentsText;
    }

    /**
     * @return the newStudentsText
     */
    public Text getNewStudentsText() {
        return newStudentsText;
    }

    /**
     * @param newStudentsText the newStudentsText to set
     */
    public void setNewStudentsText(Text newStudentsText) {
        this.newStudentsText = newStudentsText;
    }

    /**
     * @return the leftStudentsText
     */
    public Text getLeftStudentsText() {
        return leftStudentsText;
    }

    /**
     * @param leftStudentsText the leftStudentsText to set
     */
    public void setLeftStudentsText(Text leftStudentsText) {
        this.leftStudentsText = leftStudentsText;
    }

    /**
     * @return the blockedSeatsText
     */
    public Text getBlockedSeatsText() {
        return blockedSeatsText;
    }

    /**
     * @param blockedSeatsText the blockedSeatsText to set
     */
    public void setBlockedSeatsText(Text blockedSeatsText) {
        this.blockedSeatsText = blockedSeatsText;
    }

    /**
     * @return the pinnedSeatsText
     */
    public Text getPinnedSeatsText() {
        return pinnedSeatsText;
    }

    /**
     * @param pinnedSeatsText the pinnedSeatsText to set
     */
    public void setPinnedSeatsText(Text pinnedSeatsText) {
        this.pinnedSeatsText = pinnedSeatsText;
    }

    /**
     * @return the emptySeatsText
     */
    public Text getEmptySeatsText() {
        return emptySeatsText;
    }

    /**
     * @param emptySeatsText the emptySeatsText to set
     */
    public void setEmptySeatsText(Text emptySeatsText) {
        this.emptySeatsText = emptySeatsText;
    }

    /**
     * @return the pane
     */
    public GridPane getPane() {
        return pane;
    }

    /**
     * @param pane the pane to set
     */
    public void setPane(GridPane pane) {
        this.pane = pane;
    }

}
