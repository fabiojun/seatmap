/*
 * Copyright (C) 2015 Vipassana São Paulo-Brazil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package seatmap.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import seatmap.helper.Utils;
import seatmap.model.SettingsModel;
import seatmap.model.StudentModel;

/**
 *
 * @author jun
 */
public class HallController extends Controller {

    private ScrollPane hall;

    private GridPane hallPane;

    private Group group;

    private List<StudentModel> students;

    private Map<Integer, SeatController> seats;

    private List<SeatController> selectedSeats;

    private SettingsModel settings;

    private IntegerProperty totalSeats;
    private IntegerProperty oldStudents;
    private IntegerProperty newStudents;
    private IntegerProperty leftStudents;
    private IntegerProperty blockedSeats;
    private IntegerProperty pinnedStudents;
    private IntegerProperty emptySeats;

    public HallController() {
        settings = SettingsModel.getInstance();

        hallPane = new GridPane();
        hall = new ScrollPane(hallPane);

        students = new ArrayList();
        seats = new HashMap();
        selectedSeats = new ArrayList();

        this.totalSeats = new SimpleIntegerProperty();
        this.oldStudents = new SimpleIntegerProperty();
        this.newStudents = new SimpleIntegerProperty();
        this.leftStudents = new SimpleIntegerProperty();
        this.blockedSeats = new SimpleIntegerProperty();
        this.pinnedStudents = new SimpleIntegerProperty();
        this.emptySeats = new SimpleIntegerProperty();

        this.createHall();

        hallPane.setGridLinesVisible(true);
    }

    public void createHall() {

        this.createStudents();
        this.createSeats();

        hallPane.getChildren().clear();
        this.verifySeatConstraints();

        int col = 0;
        int row = 0;
        int colName = 0;
        int rowName = 0;
        int positionNumber = 0;

        hallPane.getStyleClass().clear();
        //men
        if (settings.getStartFrom().equals(SettingsModel.START_FROM_LEFT)) {

            this.addStyleClass(hallPane, "hall-pane-left");
        } else {//women
            this.addStyleClass(hallPane, "hall-pane-right");
        }

        for (int i = 0; i < settings.getSeatsQty(); i++) {

            row = settings.getRows() - i / settings.getSeatsPerRow();
            rowName = settings.getRows() - row + 1;
            //men
            if (settings.getStartFrom().equals(SettingsModel.START_FROM_LEFT)) {
                col = i % settings.getSeatsPerRow() + 1;
                colName = col;
            } else { //women
                col = settings.getSeatsPerRow() - (i % settings.getSeatsPerRow());
                colName = i % settings.getSeatsPerRow() + 1;
            }

            int position = i + 1;

            StudentModel student = students.get(i);
            SeatController seat = seats.get(position);
            seat.setStudent(student);
            seat.configDragEvent();
            seat.updatePrefSortCBO();

            hallPane.add(seat.getSeatPane(), col, row);
            if (!seat.isBlocked()) {
                positionNumber++;
                seat.getSeatText().setText("Seat: " + positionNumber + " - "
                        + Utils.getExcelColumnName(rowName) + colName);
            } else {
                seat.getSeatText().setText("");
            }

            if (settings.getFlip()) {
                seat.getSeatPane().setRotate(180);
            } else {
                seat.getSeatPane().setRotate(0);
            }

        }
    }

    private void createSeats() {

        for (int i = 0; seats.size() < settings.getSeatsQty(); i++) {

            int position = i + 1;
            SeatController seat = new SeatController(position, this);
            seats.put(position, seat);
        }
    }

    private void createStudents() {

        //Fill with max seats capacity
        if (students.size() < settings.getSeatsQty()) {
            for (int i = students.size(); i < settings.getSeatsQty(); i++) {
                StudentModel student = new StudentModel();
                students.add(student);
            }
        }

//        students.sort(null);
    }

    public void moveStudent(SeatController seatTo, SeatController seatFrom) {

        students.remove(seatFrom.getStudent());
        students.add(seatTo.getPosition() - 1, seatFrom.getStudent());
        students.remove(seatTo.getStudent());
        students.add(seatFrom.getPosition() - 1, seatTo.getStudent());
    }

    protected void verifySeatConstraints() {

        for (int i = 0; i < seats.size(); i++) {
            int position = i + 1;
            SeatController seat = seats.get(position);

            StudentModel student = null;
            if (students.size() > i) {
                student = students.get(i);
            }

            if (null != seat && null != student) {

                StudentModel prevStudent = seat.getStudent();

                if (seat.isBlocked() && !student.isEmpty()) {
                    if (prevStudent.isEmpty()) {
                        students.remove(prevStudent);
                        students.add(i, prevStudent);
                    } else {
                        students.add(i, new StudentModel());
                    }
                } else {
                    if (null != prevStudent && prevStudent.isFixed()) {
                        students.remove(prevStudent);
                        students.add(i, prevStudent);
                    }
                }
            }

        }
    }

    public void prefSortStudents() {
        this.setStudents( StudentModel.prefSortStudents(this.getStudents()));
    }
    
    public void zoom() {

        Node node = this.getHallPane();
        node.getTransforms().clear();
        Scale scale = new Scale(
                settings.getZoomLevel(),
                settings.getZoomLevel()
        );
        scale.setPivotX(0);
        scale.setPivotY(0);
        node.autosize();
        node.getTransforms().addAll(scale);
    }

    public void printMode(boolean active) {

        for (Map.Entry<Integer, SeatController> entrySet : this.getSeats().entrySet()) {
            SeatController seat = entrySet.getValue();
            seat.printMode(active);
        }
    }

    public void updateStatsProperties() {
        int totalSeats = seats.size();
        int oldStudents = 0;
        int newStudents = 0;
        int leftStudents = 0;
        int blockedSeats = 0;
        int pinnedStudents = 0;
        int emptySeats = 0;

        for (Map.Entry<Integer, SeatController> entrySet : seats.entrySet()) {
            Integer position = entrySet.getKey();
            SeatController seat = entrySet.getValue();
            StudentModel student = seat.getStudent();

            if (seat.isBlocked()) {
                blockedSeats++;
                continue;
            }
            if (student.isLeft()) {
                leftStudents++;
                continue;
            }
            if (seat.isEmpty()) {
                emptySeats++;
                continue;
            }

            if (student.isFixed()) {
                pinnedStudents++;
            }

            if (student.getCourses() > 0) {
                oldStudents++;
            } else {
                newStudents++;
            }
        }
        this.setTotalSeats(totalSeats);
        this.setOldStudents(oldStudents);
        this.setNewStudents(newStudents);
        this.setLeftStudents(leftStudents);
        this.setBlockedSeats(blockedSeats);
        this.setPinnedStudents(pinnedStudents);
        this.setEmptySeats(emptySeats);
    }

    public void scrollToBottom() {
        this.getHall().setVvalue(1.0);
    }

    /**
     * @return the hall
     */
    public ScrollPane getHall() {
        return hall;
    }

    /**
     * @param hall the hall to set
     */
    public void setHall(ScrollPane hall) {
        this.hall = hall;
    }

    /**
     * @return the hallPane
     */
    public GridPane getHallPane() {
        return hallPane;
    }

    /**
     * @param fieldsPane the hallPane to set
     */
    public void setHallPane(GridPane fieldsPane) {
        this.hallPane = fieldsPane;
    }

    /**
     * @return the students
     */
    public List<StudentModel> getStudents() {
        return students;
    }

    /**
     * @param students the students to set
     */
    public void setStudents(List<StudentModel> students) {
        this.students = students;
        this.createStudents();
    }

    public void clearStudents() {
        this.students = new ArrayList<>();
    }

    /**
     * @return the seats
     */
    public Map<Integer, SeatController> getSeats() {
        return seats;
    }

    /**
     * @param seats the seats to set
     */
    public void setSeats(Map<Integer, SeatController> seats) {
        this.seats = seats;
    }

    public void clearSeats() {
        this.seats = new HashMap<>();
    }

    /**
     * @return the selectedSeats
     */
    public List<SeatController> getSelectedSeats() {
        return selectedSeats;
    }

    /**
     * @param selectedSeats the selectedSeats to set
     */
    public void setSelectedSeats(List<SeatController> selectedSeats) {
        this.selectedSeats = selectedSeats;
    }

    /**
     * @return the leftStudents
     */
    public int getLeftStudents() {
        return leftStudents.get();
    }

    /**
     * @param leftStudents the leftStudents to set
     */
    public void setLeftStudents(int leftStudents) {
        this.leftStudents.set(leftStudents);
    }

    public IntegerProperty leftStudentsProperty() {
        return this.leftStudents;
    }

    /**
     * @return the totalSeats
     */
    public int getTotalSeats() {
        return totalSeats.get();
    }

    /**
     * @param totalSeats the totalSeats to set
     */
    public void setTotalSeats(int totalSeats) {
        this.totalSeats.set(totalSeats);
    }

    public IntegerProperty totalSeatsProperty() {
        return this.totalSeats;
    }

    /**
     * @return the oldStudents
     */
    public int getOldStudents() {
        return oldStudents.get();
    }

    /**
     * @param oldStudents the oldStudents to set
     */
    public void setOldStudents(int oldStudents) {
        this.oldStudents.set(oldStudents);
    }

    public IntegerProperty oldStudentsProperty() {
        return this.oldStudents;
    }

    /**
     * @return the newStudents
     */
    public int getNewStudents() {
        return newStudents.get();
    }

    /**
     * @param newStudents the newStudents to set
     */
    public void setNewStudents(int newStudents) {
        this.newStudents.set(newStudents);
    }

    public IntegerProperty newStudentsProperty() {
        return this.newStudents;
    }

    /**
     * @return the blockedSeats
     */
    public int getBlockedSeats() {
        return blockedSeats.get();
    }

    /**
     * @param blockedSeats the blockedSeats to set
     */
    public void setBlockedSeats(int blockedSeats) {
        this.blockedSeats.set(blockedSeats);
    }

    public IntegerProperty blockedSeatsProperty() {
        return this.blockedSeats;
    }

    /**
     * @return the pinnedStudents
     */
    public int getPinnedStudents() {
        return pinnedStudents.get();
    }

    /**
     * @param pinnedStudents the pinnedStudents to set
     */
    public void setPinnedStudents(int pinnedStudents) {
        this.pinnedStudents.set(pinnedStudents);
    }

    public IntegerProperty pinnedStudentsProperty() {
        return this.pinnedStudents;
    }

    /**
     * @return the emptySeats
     */
    public int getEmptySeats() {
        return emptySeats.get();
    }

    /**
     * @param emptySeats the emptySeats to set
     */
    public void setEmptySeats(int emptySeats) {
        this.emptySeats.set(emptySeats);
    }

    public IntegerProperty emptySeatsProperty() {
        return this.emptySeats;
    }
}
